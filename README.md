# Grouper
## a NodeJS server front end to the HEOS API
### written by hawthorn@foobox.com

A simple bit of NodeJS that adds a selection of commands that are missing from the HEOS Alexa skill, but available in the app. Specifically:

* A set of commands that group my speakers in particular combinations.
* A pair of commands that set a particular speaker to a particular volume, that work whether that speaker is in a group or not. (If you just ask Alexa to set a volume and the speaker is in a group, it sets the volume for the entire group.)

It does this through [heos-api](https://www.npmjs.com/package/heos-api). However, I was unable to get heos-api to successfully do device discovery, and I don't know why. I suspect there's something about the particular HEOS devices I have (perhaps the Denon AVS home entertainment system as well as two free-floating speakers) that the author of heos-api hasn't encountered. So to get around this, I had to hardcode the IP address (ensured through MAC reservation) of one of my devices. That's enough to get player info and send all commands to it.

It also uses Express to be a webserver. All the commands are just web pages that get loaded and cause the appropriate HEOS commands, returning a message about what they're doing.

I use DynDNS to provide a fixed DNS name that always finds my home even though my IP is dynamic. Then there's MAC reservations and port forwarding to get requests to port 3000 to go to the computer where this runs.

The last ingredient is a bunch of IFTTT applets which trigger on Alexa phrases and then call the appropriate web addresses through webhooks.

A lot of moving parts, sure, but it makes a nice slick combination.

In addition, the /stopafter page makes it listen for the next message that the player is changing songs and stops the play at that point. Unfortunately by the time it does, the next song has played a second or so, but at least it's close.
