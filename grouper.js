const heos = require('heos-api');
const express = require('express');
const app = express();
const port = 3000;
var LRPID, bedroomPID, patioPID;
var stopAfterThisSong = false;

// Note: heos-api's ability to do device discovery does not work in my house.
// To get around this, this program has a hard-coded IP address, which I fix via MAC reservation.
// By the nature of the HEOS API you only need one device to control all devices.
const IPaddress = '192.168.86.73';

function repeatString(s, num) {
	var r = '', i;
	for (i = 0; i < num ; i++) {
		r += s;
	}
	return r;
}

function padLeft(s, len, pad) {
	return repeatString(pad, len - String(s).length) + s;
}

function dateStamp() {
	var now = new Date();
	return now.getFullYear()+'-'+padLeft(now.getMonth()+1,2,'0')+'-'+padLeft(now.getDate(),2,'0') + ' ' + padLeft(now.getHours(),2,'0')+':'+padLeft(now.getMinutes(),2,'0')+':'+padLeft(now.getSeconds(),2,'0');
}

///////////////////////////////////////////////////////////////////////////////
// logCommandResult: explain what Grouper is doing to both return web page and console log
//
const logCommandResult = ((res, s) => {
	if (res) res.send(dateStamp() + ' Grouper '+ s);
	console.log(dateStamp() + ' Grouper '+ s + '\n');
});

logCommandResult(null, 'starting up');

///////////////////////////////////////////////////////////////////////////////
// ask HEOS for a list of all players and store it to get PIDs
// also register for and handle play state events
//
var connection = null;
var playerInfo = null;

const getPlayersInfo = (connection) =>
	new Promise((resolve, reject) => {
		connection.once(
			{
				commandGroup: "player",
				command: "get_players",
			},
			(response) => resolve(response)
		)
		.write("player", "get_players");
	});

heos.connect(IPaddress)
	.then((connection) => {
		// get the player info and save the relevant PIDs
		getPlayersInfo(connection)
			.then((res) => {
				playerInfo = res.payload;
				logCommandResult(null, 'has found ' + playerInfo.length + ' HEOS speakers.');
				LRPID = getPlayerPid('Living Room Stereo');
				bedroomPID = getPlayerPid('Bedroom Stereo');
				patioPID = getPlayerPid('Patio Stereo');
			})
			.catch((err) => {
				logCommandResult(null, 'aborting: failed to collect players info (' + err + ')');
				process.exit();
			});
		// register for change events so we get told when a song ends
		connection.write('system', 'register_for_change_events', { enable: 'on'});
		// whenever we get an event that indicates a now-playing change, that means a new track is starting
		connection.onAll((msg) => {
			if (msg.heos && msg.heos.command && msg.heos.command.commandGroup == 'event' && msg.heos.command.command == 'player_now_playing_changed') {
				//logCommandResult(null, '  player ' + msg.heos.message.parsed.pid + ' changed now playing');
				if (stopAfterThisSong) {
					logCommandResult(null, 'is stopping the music on ' + getPlayerName(msg.heos.message.parsed.pid) + '.');
					connection.write('player', 'set_play_state', {pid: msg.heos.message.parsed.pid, state: 'stop'});
					stopAfterThisSong = false;
				}
			}
		});
	});

///////////////////////////////////////////////////////////////////////////////
// getPlayerPid: given a device's name find its pid
//
const getPlayerPid = ((devName) => {
	var r = null;
	playerInfo.map((p) => {
		if (p.name == devName) r = p.pid;
	});
	return r;
});

///////////////////////////////////////////////////////////////////////////////
// getPlayerName: given a device's pid find its name
//
const getPlayerName = ((devPid) => {
	var r = null;
	playerInfo.map((p) => {
		if (p.pid == devPid) r = p.name;
	});
	return r;
});

///////////////////////////////////////////////////////////////////////////////
// setVolume: set a device's volume
//
const volumeSet = (connection, playerId, vol) =>
  new Promise((resolve, reject) => {
    connection
      .once(
        {
          commandGroup: "player",
          command: "set_volume",
        },
        (response) => resolve(response)
      )
      .write("player", "set_volume", { pid: playerId, level: vol });
  });

const setVolume = ((devName, volume) => {
	var playerId = getPlayerPid(devName);
	if (playerId == null) {
		logCommandResult(null, "error: cannot find pid for " + devName);
		return;
	}
	heos.connect(IPaddress)
		.then((connection) => {
			volumeSet(connection, playerId, volume)
				.then((res) => {
					logCommandResult(null, 'set ' + devName + ' volume, result=' + res.heos.result + '.');
				})
				.catch((err) => {
					logCommandResult(null, 'failed to set ' + devName + ' volume (' + err + ')');
				});
		});

});

///////////////////////////////////////////////////////////////////////////////
// setGroup: set up a configuration of grouping
// profileName can be "both" or "none" or "mastersuite" for special handling
// otherwise it's the name of the device to group with the LR HEOS
//

// each profile means a list of PIDs, with the leader first
const getPlayerIDs = ((profileName) => {
	if (profileName == 'both') return [LRPID, bedroomPID, patioPID];
	if (profileName == 'none') return [LRPID];
	if (profileName == 'none2') return [bedroomPID];
	if (profileName == 'mastersuite') return [bedroomPID, patioPID];
	if (profileName == 'Bedroom') return [LRPID, bedroomPID];
	if (profileName == 'Patio') return [LRPID, patioPID];
});

// convert the array to the form I have to send
const convertArrayToCommaList = ((playerIDs) => {
	var r = '';
	playerIDs.map((p) => r += p + ',');
	return r.slice(0,-1);
});

// group setting promise
const groupSet = (connection, playerIDlist) =>
  new Promise((resolve, reject) => {
    connection
      .once(
        {
          commandGroup: "group",
          command: "set_group",
        },
        (response) => resolve(response)
      )
      .write("group", "set_group", { pid: playerIDlist });
  });

// create and resolve the promise
const setGroup = ((profileName) => {
	var playerIDs = getPlayerIDs(profileName);
	var playerIDlist = convertArrayToCommaList(playerIDs);
	heos.connect(IPaddress)
		.then((connection) => {
			groupSet(connection, playerIDlist)
				.then((res) => {
					logCommandResult(null, 'set ' + profileName + ' group, result=' + res.heos.result + '.');
				})
				.catch((err) => {
					logCommandResult(null, 'failed to set ' + profileName + ' group (' + err + ')');
				});
		});

});


///////////////////////////////////////////////////////////////////////////////
// restart: cause Grouper to shut down (it will run in a loop, so this loads new code)
//
app.get('/restart', (req, res) => {
	logCommandResult(res, 'is now restarting.');
	setTimeout(function(){ process.exit(); }, 1000);
	// the delay gives time for asynch events to conclude
});

///////////////////////////////////////////////////////////////////////////////
// reading: set the bedroom to volume 30, whether in a group or not
//
app.get('/reading', (req, res) => {
	logCommandResult(res, 'setting the Bedroom speaker to volume 30.');
	setVolume("Bedroom",30);
});

///////////////////////////////////////////////////////////////////////////////
// shower: set the bedroom to volume 65, whether in a group or not
//
app.get('/shower', (req, res) => {
	logCommandResult(res, 'setting the Bedroom speaker to volume 65.');
	setVolume("Bedroom",65);
});

///////////////////////////////////////////////////////////////////////////////
// ungroup: undo all grouping
//
app.get('/ungroup', (req, res) => {
	logCommandResult(res, 'setting all speakers to ungrouped.');
	setGroup("none");  // breaks any living room group
	setGroup("none2"); // breaks mastersuite if it's there
	// note this won't handle it if you use the app to group and put patio in charge, which I never do, and Grouper can't do
});

///////////////////////////////////////////////////////////////////////////////
// bedroom: add bedroom to living room
//
app.get('/bedroom', (req, res) => {
	logCommandResult(res, 'adding the bedroom to the living room.');
	setGroup("Bedroom");
});

///////////////////////////////////////////////////////////////////////////////
// patio: add patio to living room
//
app.get('/patio', (req, res) => {
	logCommandResult(res, 'adding the patio to the living room.');
	setGroup("Patio");
});

///////////////////////////////////////////////////////////////////////////////
// all: add bedroom and patio to living room
//
app.get('/all', (req, res) => {
	logCommandResult(res, 'adding the bedroom and patio to the living room.');
	setGroup("both");
});

///////////////////////////////////////////////////////////////////////////////
// mastersuite: add patio to bedroom (without living room involved)
//
app.get('/mastersuite', (req, res) => {
	logCommandResult(res, 'adding the patio to the bedroom.');
	setGroup("mastersuite");
});

///////////////////////////////////////////////////////////////////////////////
// stopafter: stop after this song
//
app.get('/stopafter', (req, res) => {
	logCommandResult(res, 'will stop the music after this song.');
	stopAfterThisSong = true;
});

///////////////////////////////////////////////////////////////////////////////
// cancelstop: don't stop after this song
//
app.get('/cancelstop', (req, res) => {
	logCommandResult(res, 'will no longer stop the music after this song.');
	stopAfterThisSong = false;
});

///////////////////////////////////////////////////////////////////////////////
// start up the web listener
//
app.listen(port, () => logCommandResult(null, 'is now listening at http://localhost:' + port));
